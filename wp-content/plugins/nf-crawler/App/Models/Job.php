<?php

namespace App\Models;

use App\Models\JobFilter;
use App\Models\Model;
use App\NfCrawler\Database;

class Job extends Model
{
    protected $table = Database::DATABASE_JOB;
    public function filters()
    {
        return $this->hasMany(JobFilter::class, 'job_id');
    }
}
