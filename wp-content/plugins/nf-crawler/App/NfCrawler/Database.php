<?php

namespace App\NfCrawler;

use Exception;

class Database extends Exception
{
    const DATABASE_JOB             = 'nf_crawler_jobs';
    const DATABASE_JOB_FILTER      = 'nf_crawler_filters';
    const DATABASE_JOB_FILTER_ITEM = 'nf_crawler_filter_items';
    private $wpdb;
    public function __construct()
    {
        global $wpdb;
        $this->wpdb = $wpdb;
    }
    /**
     * Get Slider Database name
     * @return String database_name
     */
    public function getJobsTableName()
    {
        return $this->wpdb->prefix . self::DATABASE_JOB;
    }
    /**
     * Get Slider Items Database name
     * @return String database_name
     */
    public function getJobFilterTableName()
    {
        return $this->wpdb->prefix . self::DATABASE_JOB_FILTER;
    }
    /**
     * Get Slider Items Database name
     * @return String database_name
     */
    public function getJobFitlerItemsTableName()
    {
        return $this->wpdb->prefix . self::DATABASE_JOB_FILTER_ITEM;
    }
    /**
     * Create Database When Plugin is activated
     */
    public function createDatabase()
    {
        $this->createJobsTable();
        $this->createJobFilterTable();
        $this->createJobFilterItemsTable();
    }
    /**
     * Create Slider Database
     */
    public function createJobsTable()
    {
        $charset_collate = $this->wpdb->get_charset_collate();

        $sql = "CREATE TABLE {$this->getJobsTableName()} (
                        `id` int unsigned AUTO_INCREMENT PRIMARY KEY,
                        `name` varchar(255) NOT NULL,
                        `url` varchar(255) NOT NULL
                    ) $charset_collate;";
        require_once ABSPATH . 'wp-admin/includes/upgrade.php';
        dbDelta($sql);
    }
    /**
     * Create Slider Items Database
     */
    public function createJobFilterTable()
    {
        $charset_collate = $this->wpdb->get_charset_collate();

        $sql = "CREATE TABLE {$this->getJobFilterTableName()} (
                        `id` int unsigned AUTO_INCREMENT PRIMARY KEY,
                        `job_id` int unsigned,
                        `name` varchar(255) NOT NULL,
                        `number_result` boolean NOT NULL,
                        `next_filter` int NOT NULL DEFAULT 0,
                        `order` int NOT NULL DEFAULT 0,
                        FOREIGN KEY (job_id) REFERENCES {$this->getJobsTableName()}(id) 
                        ON DELETE CASCADE
                    ) $charset_collate;";
        require_once ABSPATH . 'wp-admin/includes/upgrade.php';
        dbDelta($sql);
    }
    /**
     * Create Slider Item Description Database
     */
    public function createJobFilterItemsTable()
    {
        $charset_collate = $this->wpdb->get_charset_collate();

        $sql = "CREATE TABLE {$this->getJobFitlerItemsTableName()} (
                        `id` int unsigned AUTO_INCREMENT PRIMARY KEY,
                        `filter_id` int unsigned,
                        `path` varchar(255) NOT NULL,
                        `key` varchar(50) NOT NULL,
                        `data_type` varchar(10) NOT NULL,
                        `attr` varchar(50) NOT NULL,
                        `result_key` varchar(50) NOT NULL,
                        FOREIGN KEY (filter_id) REFERENCES {$this->getJobFilterTableName()}(id)
                        ON DELETE CASCADE
                    ) $charset_collate;";
        require_once ABSPATH . 'wp-admin/includes/upgrade.php';
        dbDelta($sql);
    }
    /**
     * Get Data from database
     */
    public function find($table, $id)
    {
        $table = $this->addPrefix($table);
        $sql   = "SELECT * FROM {$table} WHERE `id` = '{$id}'";
        $data  = $this->wpdb->get_row($sql);
        if (!isset($data)) {
            throw new Exception("Can not find id = {$id}", 1001);
        }
        return $data;
    }
    /**
     * Add prefix to table name
     */
    public function addPrefix($table)
    {
        return $this->wpdb->prefix . $table;
    }
    /**
     * Save data to database
     */
    public function insert($table, $data)
    {
        $table = $this->addPrefix($table);
        $this->wpdb->insert(
            $table,
            $data
        );
        return true;
    }
    public function insertAndReturnId($table, $data)
    {
        $this->insert($table, $data);
        return $this->wpdb->insert_id;
    }
    /**
     * Update exists row
     */
    public function update($table, $data)
    {
        $table = $this->addPrefix($table);
        $id    = $data['id'];
        unset($data['id']);
        $this->wpdb->update(
            $table,
            $data,
            ['id' => $id]
        );

    }
    /**
     * get all records form a table
     *
     * @param string $table
     */
    public function all($table)
    {
        $table = $this->addPrefix($table);
        $sql   = "SELECT * FROM {$table}";
        $data  = $this->wpdb->get_results($sql);
        if (!isset($data)) {
            throw new Exception("Can not find id = {$id}", 1001);
        }
        return $data;
    }
    /**
     * get result from $sql
     *
     * @param string $sql
     */
    public function get($sql)
    {
        $data = $this->wpdb->get_results($sql);
        if (!isset($data)) {
            throw new Exception("Can not find id = {$id}", 1001);
        }
        return $data;
    }
}
