<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>VMMS</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="<?php echo THEME_URI; ?>/bower_components/jquery/dist/jquery.min.js"></script>
	<script src="<?php echo THEME_URI; ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="<?php echo THEME_URI; ?>/bower_components/slick.js/slick/slick.min.js"></script>
	<script src="<?php echo THEME_URI; ?>/js/vmms.js"></script>
	<link rel="stylesheet" href="<?php echo THEME_URI; ?>/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo THEME_URI; ?>/bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="<?php echo THEME_URI; ?>/bower_components/animate.css/animate.min.css">
	<link rel="stylesheet" href="<?php echo THEME_URI; ?>/bower_components/slick.js/slick/slick.css">
	<link rel="stylesheet" href="<?php echo THEME_URI; ?>/css/vmms.css">
	<link rel="stylesheet" href="<?php echo THEME_URI; ?>/css/custom.css">
	<link rel="stylesheet" href="<?php echo THEME_URI; ?>/bower_components/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Dancing+Script" />
	<?php wp_head(); ?>
	<!--Start of Zopim Live Chat Script-->
	<script type="text/javascript">
	window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
	d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
	_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
	$.src="//v2.zopim.com/?3VjdysBfsyZvrH9RSYEtKRBqzR6mHtom";z.t=+new Date;$.
	type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
	</script>
	<!--End of Zopim Live Chat Script-->
</head>
<body>
	<header>
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container" id="header_mobile_menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" rel="home" href="<?php echo site_url(); ?>" title="Buy Sell Rent Everyting">VICODERS</a>
				</div><!-- navbar-header -->
					<?php wp_nav_menu(['theme_location'  => 'header-menu']); ?>
			</div> 
		</nav>
	</header>
		
	