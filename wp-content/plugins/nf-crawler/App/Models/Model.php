<?php

namespace App\Models;

use App\NfCrawler\Builder\Query;
use App\NfCrawler\Database;
use Jenssegers\Model\Model as JenssegersModel;

class Model extends JenssegersModel
{
    private $database;
    public function __construct()
    {
        $this->database = new Database();
    }
    public function setData($data)
    {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
        return $this;
    }
    /**
     * Find data by id
     *
     * @param int $id
     * @return model data
     */
    public static function find($id)
    {
        $table = self::__callStatic('getTable', []);
        $model = new Model();
        $data  = $model->database->find($table, $id);
        return self::__callStatic('setData', [$data]);
    }
    /**
     * get all records
     */
    public static function all()
    {
        $table = self::__callStatic('getTable', []);
        $model = new Model();
        $data  = $model->database->all($table);
        foreach ($data as $key => $value) {
            $item       = new Model();
            $item       = self::__callStatic('setData', [$value]);
            $data[$key] = $item;
        }
        return $data;
    }
    /**
     * Insert new record to database
     *
     * @return @mixed
     */
    public function insert()
    {
        return $this->database->insert($this->getTable(), $this->attributes);
    }
    public function insertAndReturnId()
    {
        return $this->database->insertAndReturnId($this->getTable(), $this->attributes);
    }
    /**
     * update record in database
     *
     * @return @mixed
     */
    public function update()
    {
        return $this->database->update($this->getTable(), $this->attributes);
    }
    /**
     * Get model table name
     *
     * @return mixed
     */
    public function getTable()
    {
        return $this->table;
    }
    /**
     * Handle dynamic static method calls into the method.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     */
    public static function __callStatic($method, $parameters)
    {
        $instance = new static;
        return call_user_func_array([$instance, $method], $parameters);
    }
    /**
     * add hasMany function
     */
    public function hasMany($class_name, $foriegn_key)
    {
        if (!class_exists($class_name)) {
            throw new Exception("Class not found", 1002);
        } else {
            $model = new $class_name();
            $table = $this->database->addPrefix($model->getTable());
            $query = new Query();
            $sql   = $query->table($table)->where($foriegn_key, '=', $this->id)->get();
            $data = $this->database->get($sql);
            foreach ($data as $key => $value) {
                $item = new $class_name();
                $item->setData($value);
                $data[$key] = $item;
            }
            return $data;
        }
    }
}
