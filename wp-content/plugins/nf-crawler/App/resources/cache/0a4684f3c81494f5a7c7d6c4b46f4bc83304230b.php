<div id="edit-job">
	<div id="create-job">
		<div class="col-xs-12 col-md-4">
			<form method="post" action="<?php echo e(get_admin_url(null, '/admin.php?page=nf-crawler.php') . '&action=updateJob&id=' . $job->id); ?>">
				<h4>Edit</h4>
				<input type="hidden" value="<?php echo e($job->id); ?>" name="job-id" required>
				<div class="form-group">
					<label>Name</label>
					<input type="text" class="form-control" name="job-name" value="<?php echo e($job->name); ?>" required>
				</div>
				<div class="form-group">
					<label>URL</label>
					<input type="url" class="form-control" name="job-url" value="<?php echo e($job->url); ?>" required>
				</div>
				<h3>Job Filter</h3>
				<div class="filters" id="nf-crawler-filters">
					<?php foreach($job->filters as $filter): ?>
					<div class="panel panel-default filter-item">
						<div class="panel-body">
							<div class="item">
								<div class="form-group">
									<label>Name<span class="pull-right dashicons dashicons-no-alt" onclick="deleteFilter(this)"></span></label>
									<input type="text" class="form-control" name="filter-name[<?php echo e($filter->id); ?>]" value="<?php echo e($filter->name); ?>" required>
								</div>
								<div class="form-group">
									<label>Number Result</label>
									<input type="number" class="form-control" name="filter-number-result[<?php echo e($filter->id); ?>]" value="<?php echo e($filter->number_result); ?>" required>
								</div>
								<div class="form-group">
									<label>Result type</label>
									<select name="filter-result-type[<?php echo e($filter->id); ?>]" class="form-control">
										<option value="0" <?php echo e($filter->next_filter == 1 ? "selected" : ""); ?>>Create new post</option>
										<?php foreach($job->filters as $ft): ?>
										<option value="<?php echo e($ft->id); ?>" <?php echo e($ft->id == $filter->next_filter ? "selected" : ""); ?>>Filter - <?php echo e($ft->name); ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="form-group <?php echo e(count($filter->items) == 0 ? 'hidden' : ''); ?>">
									<label>Edit Filter Items</label>
									<?php foreach($filter->items as $item): ?> 
										<?php echo $__env->make('admin._filter_item_edit', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
									<?php endforeach; ?>
								</div>
								<hr>
								<div id="nf-crawl-filter-items">
									<label>New Filter Items</label>
									<div class="form-group">
										<button class="button button-default" type="button" onclick="addMoreFilterItem(this, <?php echo e($filter->id); ?>)">Add More Filter Item</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php endforeach; ?>
					<div class="form-group" id="add-more-filter">
						<button class="button button-default" type="button" onclick="addMoreFilter()">Add More Filter</button>
					</div>
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Save">
				</div>
			</form>
		</div>
	</div>
</div>

<div id="new-filter" class="hidden">
	<div class="panel panel-default filter-item">
		<div class="panel-body">
			<div class="item">
				<div class="form-group">
					<label>Name<span class="pull-right dashicons dashicons-no-alt" onclick="deleteFilter(this)"></span></label>
					<input type="text" class="form-control" name="filter-name[new][]" required>
				</div>
				<div class="form-group">
					<label>Number result</label>
					<input type="number" class="form-control" name="filter-number-result[new][]" required>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="new-filter-item" class="hidden">
	<?php echo $__env->make('admin._filter_item_create', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>
