<div id="edit-job">
	<div id="create-job">
		<div class="col-xs-12 col-md-4">
			<form method="post" action="{{ get_admin_url(null, '/admin.php?page=nf-crawler.php') . '&action=updateJob&id=' . $job->id }}">
				<h4>Edit</h4>
				<input type="hidden" value="{{ $job->id }}" name="job-id" required>
				<div class="form-group">
					<label>Name</label>
					<input type="text" class="form-control" name="job-name" value="{{ $job->name }}" required>
				</div>
				<div class="form-group">
					<label>URL</label>
					<input type="url" class="form-control" name="job-url" value="{{ $job->url }}" required>
				</div>
				<h3>Job Filter</h3>
				<div class="filters" id="nf-crawler-filters">
					@foreach($job->filters as $filter)
					<div class="panel panel-default filter-item">
						<div class="panel-body">
							<div class="item">
								<div class="form-group">
									<label>Name<span class="pull-right dashicons dashicons-no-alt" onclick="deleteFilter(this)"></span></label>
									<input type="text" class="form-control" name="filter-name[{{$filter->id}}]" value="{{ $filter->name }}" required>
								</div>
								<div class="form-group">
									<label>Number Result</label>
									<input type="number" class="form-control" name="filter-number-result[{{$filter->id}}]" value="{{ $filter->number_result }}" required>
								</div>
								<div class="form-group">
									<label>Result type</label>
									<select name="filter-result-type[{{$filter->id}}]" class="form-control">
										<option value="0" {{ $filter->next_filter == 1 ? "selected" : "" }}>Create new post</option>
										@foreach($job->filters as $ft)
										<option value="{{$ft->id}}" {{ $ft->id == $filter->next_filter ? "selected" : "" }}>Filter - {{$ft->name}}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group {{ count($filter->items) == 0 ? 'hidden' : '' }}">
									<label>Edit Filter Items</label>
									@foreach($filter->items as $item) 
										@include('admin._filter_item_edit')
									@endforeach
								</div>
								<hr>
								<div id="nf-crawl-filter-items">
									<label>New Filter Items</label>
									<div class="form-group">
										<button class="button button-default" type="button" onclick="addMoreFilterItem(this, {{$filter->id}})">Add More Filter Item</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					@endforeach
					<div class="form-group" id="add-more-filter">
						<button class="button button-default" type="button" onclick="addMoreFilter()">Add More Filter</button>
					</div>
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Save">
				</div>
			</form>
		</div>
	</div>
</div>

<div id="new-filter" class="hidden">
	<div class="panel panel-default filter-item">
		<div class="panel-body">
			<div class="item">
				<div class="form-group">
					<label>Name<span class="pull-right dashicons dashicons-no-alt" onclick="deleteFilter(this)"></span></label>
					<input type="text" class="form-control" name="filter-name[new][]" required>
				</div>
				<div class="form-group">
					<label>Number result</label>
					<input type="number" class="form-control" name="filter-number-result[new][]" required>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="new-filter-item" class="hidden">
	@include('admin._filter_item_create')
</div>
