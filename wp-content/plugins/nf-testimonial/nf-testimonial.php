<?php
/*
Plugin Name: NF Testimonial
Plugin URI:  http://codersvn.com/wp/plugins/nf-testimonial
Description: This plugin will help you create an accordion menu.
Version:     1.0.0
Author:      Hieupv
Author URI:  http://codersvn.com/wp/plugins/nf-testimonial
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Domain Path: http://codersvn.com/wp/plugins/nf-testimonial
Text Domain: http://codersvn.com/wp/plugins/nf-testimonial
 */

defined('ABSPATH') or die();

require __DIR__ . '/vendor/autoload.php';

class NfTestimonial
{
    public function __construct()
    {
        add_action('widgets_init', function () {
            register_widget('NfTestimonialWidget');
        });
        if (is_admin()) {
            add_action('admin_head', [$this, 'pluginStyle']);
            add_action('admin_enqueue_scripts', function () {
                wp_enqueue_media();
            });
        }

    }
    public function pluginStyle()
    {
        $output = '<link rel="stylesheet" href="' . plugins_url('assets/css/plugin.css', __FILE__) . '">';
        $output .= '<script type="text/javascript" src="' . plugins_url('assets/js/plugin.js', __FILE__) . '"></script>';
        echo $output;
    }
}
new NfTestimonial;
