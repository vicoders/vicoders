<?php get_header(); ?>
	<?php while(have_posts()) : the_post() ?>
		<!-- <div class="single-blog-title"><h1><?php the_title(); ?></h1></div>
		<div class="single-blog-content"><?php the_content(); ?></div> -->

		<div id="portfolio" class="container">
			<div class="title"><h1><?php the_title(); ?></h1></div>
			<div class="col-lg-12 col-md-8 col-sm-7 col-xs-12">
				<div class="single-blog-content"><?php the_content(); ?></div>
			</div>
		</div>


		
	<?php endwhile; ?>
<?php get_footer(); ?>
