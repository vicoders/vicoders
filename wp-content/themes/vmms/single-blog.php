<?php get_header(); ?>
	<div id="single-blog" class="container">
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<div class="single-blog">			   	
				<?php while(have_posts()) : the_post() ?>
					<div class="single-blog-title"><h1><?php the_title(); ?></h1></div>
					<div class="single-blog-content"><?php the_content(); ?></div>
				<?php endwhile; ?>
			</div>					
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<div class="background-white">
				<?php dynamic_sidebar('blog-sidebar');	?>	
			</div>
		</div>
	</div>	
<?php get_footer(); ?>