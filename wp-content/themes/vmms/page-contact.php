<?php get_header(); ?>
     <div id="contact-page">
        <div class="container">
            <div class="col-lg-12"> 
                 <h1 class="text-center"><?php the_title(); ?></h1>             
               
                <div class="col-lg-7 col-md-6 col-sm-8 col-xs-12 ">                
                     
                    <img class="languageLogo" src="<?php echo THEME_URI; ?>/images/contact-pic.png">
        
                </div>
                <div class="col-lg-5 col-md-6 col-sm-8 col-xs-12 ">                   
                    <div class="contact-page">
                        <?php while(have_posts()) : the_post() ?>
                            <?php the_content(); ?>
                        <?php endwhile; ?>
                    </div>
                </div>
                 <!-- <div class="col-lg-3 col-md-6 col-sm-8 col-xs-12 ">                
                    <div class="contact-page"> 
                       <?php dynamic_sidebar('footer-1');   ?>
                    </div>
                </div> -->
            </div> <!-- end col-lg-12 -->
        </div> <!-- end container -->
    </div>  <!-- end contact-page --> 
			
<?php get_footer(); ?>




