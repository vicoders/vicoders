<?php
/*
Plugin Name: NightFury Bootstrap Menu
Plugin URI:  http://wp.vietnamdev.com/plugins/nightfury-bootstrap-menu
Description: This plugin will help you create an accordion menu.
Version:     1.0.0
Author:      Night Fury
Author URI:  http://wp.vietnamdev.com/plugins/nightfury-bootstrap-menu
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Domain Path: http://wp.vietnamdev.com/plugins/nightfury-bootstrap-menu
Text Domain: http://wp.vietnamdev.com/plugins/nightfury-bootstrap-menu
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

define('NIGHTFURY_BOOTSTRAP_OPTION_KEY', 'bootstrap_accordion_option');
/*
* Bootstrap Accordion Menu Walker
*/


require_once(__DIR__.'/bootstrap_accordion_walker.php');

/*
* NightFury Bootstrap Navbar Walker
*/
require_once(__DIR__.'/boostrap_navbar_walker.php');

/*
* Add Option Page
*/

add_action('admin_menu', 'bootstrap_accordion_menu_option_menu');

function bootstrap_accordion_menu_option_menu() {
	add_options_page('NightFury Bootstrap Menu', 'NightFury Bootstrap Menu', 'manage_options', 'nightfury-bootstrap-menu-settings', 'nightfury_bootstrap_menu_option_page');
}

require_once(__DIR__.'/nightfury_bootstrap_option_page.php');


function modify_nav_menu_args( $args )
{
	$NightFuryOption = get_option(NIGHTFURY_BOOTSTRAP_OPTION_KEY,'false');
	if( array_key_exists($args['theme_location'], $NightFuryOption) && $NightFuryOption[$args['theme_location']] != '' )
	{
		if($NightFuryOption[$args['theme_location']] == 'BootstrapAccordionMenuWalker')
		{
			$args['walker'] = new BootstrapAccordionMenuWalker;
		}
		else if($NightFuryOption[$args['theme_location']] == 'NightFuryBootstrapNavbar')
		{
			$args['walker']          = new NightFuryBootstrapNavbar;
			$args['container']       = 'div';
			$args['container_class'] = 'collapse navbar-collapse';
			$args['menu_class']      = 'nav navbar-nav navbar-right';
		}
	}

	return $args;
}

add_filter( 'wp_nav_menu_args', 'modify_nav_menu_args' );
?>