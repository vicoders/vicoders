<?php

namespace App\NfCrawler;

use Exception;
use Symfony\Component\DomCrawler\Crawler;

class Crawl
{
    private $url;
    private $crawler;
    public function __construct()
    {
    }
    public function setUrl($url)
    {
        $this->url     = $url;
        $html          = file_get_contents($url);
        $this->crawler = new Crawler();
        $this->crawler->addContent($html);
    }
    public function crawl($filter)
    {
        $data = [];
        if ($filter->number_result == 1) {
            $data = $this->filter_single_value($filter);
        } else {
            $data = $this->filter_multi_value($filter);
        }
        return $data;
    }
    /**
     * Crawl data
     *
     * @param Crawler/filter $craw_data, JobFilter $filter
     * @return mixed
     */
    private function filter_single_value($filter)
    {
        $data = [];
        foreach ($filter->items as $key => $item) {
            try {
                $crawl_data = $this->crawler->filter($item->path);
            } catch (Exception $e) {
                return $e->getMessage();
            }
            if (!isset($data)) {
                $data = [];
            }
            $data[$item->key] = $crawl_data->{$item->data_type}($item->attr);
        }
        return $data;
    }
    /**
     * Crawl data
     *
     * @param Crawler/filter $craw_data, JobFilter $filter
     * @return mixed
     */
    private function filter_multi_value($filter)
    {
        $filter_data = [];
        foreach ($filter->items as $item) {
            $crawl_data = $this->crawler->filter($item->path);

            $d = $crawl_data->each(function (Crawler $node, $i) use ($item) {
                return $node->{$item->data_type}($item->attr);
            });
            // ignore splice results if number_result == 0
            if ($filter->number_result != 0) {
                array_splice($d, $filter->number_result - 1);
            }
            if (in_array($item->attr, ['src', 'href'])) {
                foreach ($d as $key => $url) {
                    $d[$key] = $this->urlFilter($url);
                }
            }
            $filter_data[$item->key] = $d;
        }
        $data = [];
        foreach ($filter_data as $key => $value) {
            $node_length = count($value);
            for ($i = 0; $i < $node_length; $i++) {
                if (!isset($data[$i])) {
                    $data[$i] = [];
                }
                $data[$i][$key] = $value[$i];
            }
        }
        return $data;
    }
    /**
     * Url filter - add static prefix to relative url
     *
     * @param string $url
     * @return string $url
     */
    public function urlFilter($url)
    {
        try {
            $end_with_slash        = '/\/$/';
            $start_with_slash      = '/^\//';
            $domain_without_scheme = '/^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}+/';
            try {
                $base_url = $this->crawler->filter('base')->attr('href');
            } catch (Exception $e) {
                $base_url = $this->getUrl();
            }
            $parse = parse_url($url);
            if (!isset($parse['scheme'])) {
                if (preg_match($domain_without_scheme, $url)) {
                    $parse_job_url = parse_url($base_url);
                    $url           = $parse_job_url['scheme'] . '://' . $url;
                } else {
                    if (preg_match($start_with_slash, $url)) {
                        $url = parse_url($base_url, PHP_URL_SCHEME) . '://' . parse_url($base_url, PHP_URL_HOST) . $url;
                    } else {
                        if (preg_match($end_with_slash, $base_url)) {
                            $url = $base_url . $url;
                        } else {
                            $url = $base_url . '/' . $url;
                        }
                    }
                }
            }
            return $url;
        } catch (Exception $e) {
            return $url;
        }
    }

    /**
     * Gets the value of url.
     *
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }
}
