<?php

class NfTestimonialWidget extends WP_Widget
{
    public function __construct()
    {
        parent::__construct(
            'testimonial',
            __('Testimonial', 'testimonial'),
            array('description' => __('Testimonial', 'testimonial'))
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget($args, $instance)
    {
        include __DIR__ . '/../templates/widget.php';
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form($instance)
    {
        include __DIR__ . '/../templates/widget-admin.php';
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update($new_instance, $old_instance)
    {
        $instance['image']       = (!empty($new_instance['image'])) ? strip_tags($new_instance['image']) : '';
        $instance['name']        = (!empty($new_instance['name'])) ? strip_tags($new_instance['name']) : '';
        $instance['testimonial'] = (!empty($new_instance['testimonial'])) ? strip_tags($new_instance['testimonial']) : '';
        return $instance;
    }

}
