jQuery(document).ready(function($) {
    $(document).on("click", ".upload_image_button", function() {

        jQuery.data(document.body, 'thisElement', $(this));

        window.send_to_editor = function(html) {
            var imgurl = jQuery('img', html).attr('src');

            var thisElement = jQuery.data(document.body, 'thisElement');

            thisElement.find('img').attr('src', imgurl);

            tb_remove();
        };

        tb_show('', 'media-upload.php?type=image&TB_iframe=true');
        return false;
    });
});

function AddText() {
    var InputText = document.getElementById('text-data');
    if (InputText == undefined || InputText.value == '') {
        return;
    } else {
        var li = document.createElement('li');
        li.innerHTML = InputText.value;
        console.log(li);
        document.getElementById('nf-text-list').appendChild(li);
        InputText.value = '';
    };
};

function AddNewSlide() {
    var image = document.getElementById('image-data');
    if (image == undefined || image.src == '') {
        return;
    } else {
        var data = new Object();
        data.image = image.src;
        var TextElement = document.getElementById('nf-text-list');
        var nodes = TextElement.childNodes;
        data.textData = new Array();
        for (i = 0; i < nodes.length; i++) {
            data.textData.push(nodes[i].innerHTML);
        }
        console.log(data);
    };
    var form = document.createElement('form');
    form.method = 'POST';
    var input = document.createElement('input');
    input.name = 'image';
    input.value = data.image;
    form.appendChild(input);
    data.textData.forEach(function(value, key) {
        var input = document.createElement('input');
        input.name = 'text[]';
        input.value = value;
        form.appendChild(input);
    });
    form.submit();
};

function DeleteSlide(slide_id) {
    var form = document.createElement('form');
        form.method = 'POST';
    var input = document.createElement('input');
        input.name = 'delete_slide_id';
        input.value = slide_id;
        form.appendChild(input);
    form.submit();
};  