<?php

use App\NfCrawler\Admin;
use App\NfCrawler\Api;
use App\NfCrawler\Database;
use Jenssegers\Blade\Blade;

/*
Plugin Name: NF Crawler
Plugin URI:  http://codersvn.com/wordpress/plugins/nf-crawler
Description: This plugin will help you create an accordion menu.
Version:     1.0.0
Author:      Night Fury
Author URI:  http://codersvn.com/wordpress/plugins/nf-crawler
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Domain Path: http://codersvn.com/wordpress/plugins/nf-crawler
Text Domain: http://codersvn.com/wordpress/plugins/nf-crawler
 */
?>
<?php
require __DIR__ . '/vendor/autoload.php';

class NfCrawler
{
    private $database;
    private $admin;
    public function __construct()
    {
        $this->database = new Database();
        $views          = __DIR__ . '/App/resources/views';
        $cache          = __DIR__ . '/App/resources/cache';
        $blade          = new Blade($views, $cache);
        $this->admin    = new Admin($blade);

        if (is_admin()) {
            register_activation_hook(__FILE__, [$this->database, 'createDatabase']);
        }
    }
}
new NfCrawler();
new Api();
