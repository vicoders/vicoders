<?php get_header(); ?>
	<div id="portfolio" class="container">
		<div class="title"><h1>Portfolio</h1></div>
			<div class="col-lg-12 col-md-8 col-sm-7 col-xs-12">			
				<?php $args = array(
					'posts_per_page'   => 100,
					'post_type' => 'portfolio',
					'paged' => 1,
				);
				$posts = new WP_Query( $args ); 

				while($posts->have_posts()) : $posts->the_post()
				?>
					<div class="col-lg-4" ng-click="LoadWorkPage('Adam-Eve-App')" style="padding-bottom:20px;">
						<div class="portfolio">
							<div class="image">
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
									<?php echo get_the_post_thumbnail($post->ID,'thumbnail'); ?>
								</a>
							</div>
							<div class="p_row">
								<div class="time">
									<div class="date"><?php echo '<span>'.get_the_date('j', $post->ID).'</span><span>'.get_the_date('F', $post->ID).'</span>'; ?></div>
								</div>
								<div class="post-title">
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
										<p><?php the_title(); ?></p>
										<p class="author">Post by: <?php the_author(); ?>, <?php echo get_the_date('H:i', $post->ID); ?></p>
									</a>
								</div>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
				<?php wp_pagenavi(); ?>
				</div>
				
	</div>
<?php get_footer(); ?>


