<div id="list-jobs">
    <form id="posts-filter" method="get">
        <div class="tablenav top">
            <div class="alignleft actions bulkactions">
                <label for="bulk-action-selector-top" class="screen-reader-text">Select bulk action</label>
                <select name="action" id="bulk-action-selector-top">
                    <option value="-1">Bulk Actions</option>
                    <option value="edit" class="hide-if-no-js">Edit</option>
                    <option value="trash">Move to Trash</option>
                </select>
                <input type="submit" id="doaction" class="button action" value="Apply">
            </div>
            <div class="alignleft actions">
                <label for="filter-by-date" class="screen-reader-text">Filter by date</label>
                <select name="m" id="filter-by-date">
                    <option selected="selected" value="0">All dates</option>
                    <option value="201512">December 2015</option>
                    <option value="201506">June 2015</option>
                </select>
                <input type="submit" name="filter_action" id="post-query-submit" class="button" value="Filter"> </div>
            <div class="tablenav-pages one-page"><span class="displaying-num">6 items</span>
                <span class="pagination-links"><span class="tablenav-pages-navspan" aria-hidden="true">«</span>
                <span class="tablenav-pages-navspan" aria-hidden="true">‹</span>
                <span class="paging-input"><label for="current-page-selector" class="screen-reader-text">Current Page</label><input class="current-page" id="current-page-selector" type="text" name="paged" value="1" size="1" aria-describedby="table-paging"> of <span class="total-pages">1</span></span>
                <span class="tablenav-pages-navspan" aria-hidden="true">›</span>
                <span class="tablenav-pages-navspan" aria-hidden="true">»</span></span>
            </div>
            <br class="clear">
        </div>
        <h2 class="screen-reader-text">Pages list</h2>
        <table class="wp-list-table widefat fixed striped pages">
            <thead>
                <tr>
                    <td id="cb" class="manage-column column-cb check-column">
                        <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
                        <input id="cb-select-all-1" type="checkbox">
                    </td>
                    <th scope="col" id="title" class="manage-column column-title column-primary sortable desc"><a href="#"><span>Name</span><span class="sorting-indicator"></span></a></th>
                    <th scope="col" id="author" class="manage-column column-author">Created By</th>
                    <th scope="col" id="comments" class="manage-column column-comments num sortable desc"><a href="#"><span><span class="vers comment-grey-bubble" title="Comments"><span class="screen-reader-text">Comments</span></span></span><span class="sorting-indicator"></span></a></th>
                    <th scope="col" id="date" class="manage-column column-date sortable asc"><a href=""><span>Date</span><span class="sorting-indicator"></span></a></th>
                </tr>
            </thead>
            <tbody id="the-list">
                <?php foreach($jobs as $job): ?>
                <tr id="post-9" class="iedit author-self level-0 post-9 type-page status-publish hentry">
                    <th scope="row" class="check-column">
                        <label class="screen-reader-text" for="cb-select-9">Select About</label>
                        <input id="cb-select-9" type="checkbox" name="post[]" value="9">
                        <div class="locked-indicator"></div>
                    </th>
                    <td class="title column-title has-row-actions column-primary page-title" data-colname="Title"><strong><a class="row-title" href="<?php echo e(get_admin_url(null, '/admin.php?page=nf-crawler.php') . '&action=edit&id=' . $job->id); ?>"><?php echo e($job->name); ?></a></strong>
                        <div class="locked-info">
                            <span class="locked-avatar"></span> <span class="locked-text"></span>
                        </div>
                        <div class="row-actions">
                            <span class="edit"><a onclick="run(<?php echo e($job->id); ?>)">Run</a> | </span><span class="trash"><a class="submitdelete" href="#">Trash</a></span></div>
                    </td>
                    <td class="author column-author" data-colname="Author"><a href="edit.php?post_type=page&amp;author=1">admin@vmms</a></td>
                    <td class="comments column-comments" data-colname="Comments">
                        <div class="post-com-count-wrapper">
                            <span aria-hidden="true">—</span><span class="screen-reader-text">No comments</span><span class="post-com-count post-com-count-pending post-com-count-no-pending"><span class="comment-count comment-count-no-pending" aria-hidden="true">0</span><span class="screen-reader-text">No comments</span></span>
                        </div>
                    </td>
                    <td class="date column-date" data-colname="Date">Published
                        <br>
                        <abbr title="2015/06/30 1:37:45 am">2015/06/30</abbr>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </form>
</div>