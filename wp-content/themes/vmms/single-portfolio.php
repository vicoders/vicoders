<?php get_header(); ?>
	<div class="container" id="single-portfolio">
		<div class="col-md-8 col-sm-6 col-xs-12">
			<div class="background-white">
				<?php while(have_posts()) : the_post() ?>
					<p class="page-title"><?php the_title(); ?></p>
					<div class="content"><?php the_content(); ?></div>
				<?php endwhile; ?>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="background-white">
				<?php dynamic_sidebar('portfolio-sidebar');	?>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
