<div id="create-job">
	<div class="col-xs-12 col-md-4">
		<form method="post" action="{{ get_admin_url(null, '/admin.php?page=nf-crawler.php') . '&action=createJob' }}">
			<h4>Create new job</h4>
			<div class="form-group">
				<label>Name</label>
				<input type="text" class="form-control" name="job-name" required>
			</div>
			<div class="form-group">
				<label>URL</label>
				<input type="url" class="form-control" name="job-url" required>
			</div>
			<h3>Job Filter</h3>
			<div class="filters" id="nf-crawler-filters">
				<div class="panel panel-default filter-item">
					<div class="panel-body">
						<div class="item">
							<div class="form-group">
								<label>Name<span class="pull-right dashicons dashicons-no-alt" onclick="deleteFilter(this)"></span></label>
								<input type="text" class="form-control" name="filter-name[]" required>
							</div>
							<div class="form-group">
								<label>Number result</label>
								<input type="number" class="form-control" name="filter-number-result[]" required>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group" id="add-more-filter">
					<button class="button button-default" type="button" onclick="addMoreFilter()">Add More Filter</button>
				</div>
			</div>
			<div class="form-group">
				<input type="submit" class="btn btn-primary" value="Create">
			</div>
		</form>
	</div>
</div>
