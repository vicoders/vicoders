<?php 
class NightFuryBootstrapNavbar extends Walker_Nav_Menu {
	function start_lvl( &$output, $item, $depth = 0, $args = array(), $id = 0 )
	{
		$output .= '<ul class="dropdown-menu" role="menu">';
	}
	function end_lvl( &$output, $item, $depth = 0, $args = array(), $id = 0 )
	{
		$output .= '</ul>';
	}
	function start_el( &$output, $item, $depth, $args )
	{
		if($item->menu_item_parent == 0)
		{
			if($item->attr_title == 'parent')
			{
				$output .= '<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">'. $item->title;
			}
			else
			{
				$output .= '<li><a href="'.$item->url.'">'. $item->title;
			}
		}
		else
		{
			$output .= '<li><a href="'.$item->url.'">'. $item->title;
		}
	}
	function end_el( &$output, $item, $depth, $args )
	{
		$output .= "</a></li>\n";
	}
}
?>