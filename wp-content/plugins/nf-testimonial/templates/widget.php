<div class="comment paralax invisible col-xs-12 col-md-6">
	<div class="avatar">
		<img src="<?php echo $instance['image'] ?>" alt="<?php echo $instance['name']; ?>">
	</div>
	<div class="name">
		<p><?php echo $instance['name']; ?></p>
	</div>
	<div class="comment-content">
		<p><?php echo $instance['testimonial']; ?></p>
	</div>
</div>