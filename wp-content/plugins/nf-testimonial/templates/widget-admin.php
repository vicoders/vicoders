<div class="image-category-widget">
    <div class="widget-image">
        <img src="<?php echo isset($instance['image']) ? $instance['image'] : ''; ?>">
    </div>
    <input type="hidden" id="<?php echo $this->get_field_id('image');?>" name="<?php echo $this->get_field_name('image');?>" value="<?php echo isset($instance['image']) ? $instance['image'] : ''; ?>">
    <button class="set_custom_images button">Choose Image</button>
    <div class="form-group">
        <label>Name</label>
        <input type="<?php echo $this->get_field_id('name');?>" class="form-control" name="<?php echo $this->get_field_name('name');?>" value="<?php echo isset($instance['name']) ? $instance['name'] : ''; ?>">
    </div>
    <div class="form-group">
        <label>Testimonial</label>
        <textarea type="<?php echo $this->get_field_id('testimonial');?>" class="form-control" name="<?php echo $this->get_field_name('testimonial');?>"><?php echo isset($instance['testimonial']) ? $instance['testimonial'] : ''; ?></textarea>
    </div>
</div>