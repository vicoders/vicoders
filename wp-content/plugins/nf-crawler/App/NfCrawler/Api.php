<?php

namespace App\NfCrawler;

use App\Models\Job;
use App\Models\JobFilter;
use App\NfCrawler\Crawl;
use App\NfCrawler\Post;
use Exception;

class Api
{
    public function __construct()
    {
        @set_exception_handler([$this, 'exceptionHandler']);
        if (is_admin()) {
            add_action('wp_ajax_job', [$this, 'getJob']);
            add_action('wp_ajax_run_filter', [$this, 'runFilter']);
        }
    }
    public function exceptionHandler($e)
    {
        return $this->error($e);
    }
    /**
     * Run a filter and return data
     *
     * @param string $_POST['filter_id'], string $_POST['user_id'], array $_POST['category_id'], string $_POST['url']
     * @return mixed
     */
    public function runFilter()
    {
        try {
            $filter_id     = $_POST['filter_id'];
            $user_id       = isset($_POST['user_id']) ? $_POST['user_id'] : get_current_user_id();
            $post_type     = isset($_POST['post_type']) ? $_POST['post_type'] : 'post';
            $category      = isset($_POST['category_ids']) ? $_POST['category_ids'] : [];
            $filter        = JobFilter::find($filter_id);
            $filter->items = $filter->items();
            $url           = $_POST['url'];
            $crawl         = new Crawl();
            $crawl->setUrl($url);
            if ($filter->next_filter == 0) {
                $data = $crawl->crawl($filter);
                $post = [
                    'post_title'    => $data['post_title'],
                    'post_content'  => $data['post_content'],
                    'post_image'    => $data['post_image'],
                    'post_status'   => $_POST['post_status'],
                    'post_author'   => $user_id,
                    'post_category' => $category,
                    'post_type'     => $post_type,
                ];
                $post_id = Post::create($post);
                return $this->response();
            } else {
                $data = $crawl->crawl($filter);
                return $this->response($data);die();
                return $this->response($data);
            }
        } catch (Exception $e) {

        }
    }
    /**
     * Get job data
     *
     * @param $_POST['job_id']
     * @return mixed
     */
    public function getJob()
    {
        if (!isset($_POST['job_id']) || $_POST['job_id'] == '') {
            throw new Exception("can not find job_id", 1006);
        } else {
            $job_id  = $_POST['job_id'];
            $job     = Job::find($job_id);
            $filters = $job->filters();
            foreach ($filters as $key => $value) {
                $filters[$key]->items = $value->items();
                if ($value->next_filter != 0) {
                    $filters[$key]->next_filter = $this->getFilter($value->next_filter);
                }
            }
            $job->filters = $filters;
            return $this->response($job);
        }
    }
    /**
     * Get Filter Data
     *
     * @param string $_POST['filter_id']
     * @return mixed
     */
    private function getFilter($filter_id)
    {
        $filter = JobFilter::find($filter_id);
        if ($filter->next_filter != 0) {
            $filter->next_filter = $this->getFilter($filter->next_filter);
        }
        return $filter;
    }
    /**
     * Return Data if success
     *
     * @param $data
     * @return array ['data' => $data] with HTTP/1.0 200 OK in header
     */
    public function response($data = null)
    {
        header('HTTP/1.0 200 OK');
        header('Content-Type: application/json');
        if (isset($data)) {
            $output = json_encode(['data' => $data]);
        } else {
            $output = json_encode(['data' => 'success']);
        }
        echo $output;
        die();
    }
    /**
     * Return Data if success
     *
     * @param $data
     * @return array ['error' => {error_message}, 'error_code' => {error_code}] with HTTP/1.0 500 Error in header
     */
    public function error($e)
    {
        header('HTTP/1.0 500 Error');
        header('Content-Type: application/json');
        if (isset($e)) {
            $output = json_encode(['error' => $e->getMessage(), 'error_code' => $e->getCode()]);
        } else {
            $output = json_encode(['error' => 'error']);
        }
        echo $output;
        die();
    }
}
