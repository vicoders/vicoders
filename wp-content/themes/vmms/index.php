<?php get_header(); ?>
	<div id="landing">
		<div class="bg-opacity">	
			<div class="fit-height">
				<div class="container">
					<div id="slogan">
						<p class="text-center">Instant help from expert developers</p>
					</div>
				</div>
				<?php if(function_exists('nightfury_portfolio_slider')) { nightfury_portfolio_slider(); } ?>
			</div>
			<div id="intro-block">
				<div class="container paralax invisible">
					<div class="col-md-3 col-sm-6 col-xs-12">
						<?php dynamic_sidebar('block-intro-1'); ?>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<?php dynamic_sidebar('block-intro-2'); ?>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<?php dynamic_sidebar('block-intro-3'); ?>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<?php dynamic_sidebar('block-intro-4'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="tech-logo">
		<div class="container text-center">
			<a href="#">
				<img class="languageLogo" src="<?php echo THEME_URI; ?>/images/techlogo/ruby-on-rails.png">
			</a>
			<a href="#">
				<img class="languageLogo" src="<?php echo THEME_URI; ?>/images/techlogo/javascript.png">
			</a>
			<a href="#">
				<img class="languageLogo" src="<?php echo THEME_URI; ?>/images/techlogo/angularjs.png">
			</a>
			<a href="#">
				<img class="languageLogo" src="<?php echo THEME_URI; ?>/images/techlogo/swift.png">
			</a>
			<a href="#">
				<img class="languageLogo" src="<?php echo THEME_URI; ?>/images/techlogo/ios.png">
			</a>
			<a href="#">
				<img class="languageLogo" src="<?php echo THEME_URI; ?>/images/techlogo/android.png">
			</a>
			<a href="#">
				<img class="languageLogo" src="<?php echo THEME_URI; ?>/images/techlogo/python.png">
			</a>
			<a href="#">
				<img class="languageLogo" src="<?php echo THEME_URI; ?>/images/techlogo/html_css.png">
			</a>
			<a href="#">
				<img class="languageLogo" src="<?php echo THEME_URI; ?>/images/techlogo/nodejs.png">
			</a>
			<a href="#">
				<img class="languageLogo" src="<?php echo THEME_URI; ?>/images/techlogo/ember-js.png">
			</a>
			<a href="#">
				<img class="languageLogo" src="<?php echo THEME_URI; ?>/images/techlogo/reactjs.png">
			</a>
			<a href="#">
				<img class="languageLogo" src="<?php echo THEME_URI; ?>/images/techlogo/wordpress.png">
			</a>
			<a href="#">
				<img class="languageLogo" src="<?php echo THEME_URI; ?>/images/techlogo/php.png">
			</a>
			<a href="#">
				<img class="languageLogo" src="<?php echo THEME_URI; ?>/images/techlogo/docker.png">
			</a>
		</div>
	</div>
	<div id="portfolio-list" class="paralax text-center group">
		<div class="container paralax invisible">
			<div class="col md-12 col-xs-12 col-sm-12">
				<div class="title"><?php dynamic_sidebar('portfolio-title') ?></div>
				<div class="list" id="portfolio-slick">
					<?php 
						$posts = getPortfolio();
						while($posts->have_posts()) : $posts->the_post()
					?>
					<div class="portfolio">
						<div class="image">
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<?php echo get_the_post_thumbnail($post->ID,'thumbnail'); ?>
							</a>
						</div>
						<div class="p_row">
							<div class="time">
								<div class="date"><?php echo '<span>'.get_the_date('j', $post->ID).'</span><span>'.get_the_date('F', $post->ID).'</span>'; ?></div>
							</div>
							<div class="post-title">
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
									<p><?php the_title(); ?></p>
									<p class="author">Post by: <?php the_author(); ?>, <?php echo get_the_date('H:i', $post->ID); ?></p>
								</a>
							</div>
						</div>
					</div>
					<?php endwhile ?>
				</div>
			</div>
		</div>
		<div class="container">
			<div id="feedback" class="col-md-12 col-sm-12 col-sm-12">
				<div class="title paralax invisible"><?php dynamic_sidebar('comment-box-title') ?></div>
				<?php dynamic_sidebar('testimonials'); ?>
			</div>
		</div>
		<div class="container">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<?php dynamic_sidebar('people-of-company'); ?>
			</div>
		</div>
	</div>
	<script>
	    $(document).ready(function() {
            $("#feedback .comment .name").css("width", "12%");
	    });
	</script>
<?php get_footer(); ?>