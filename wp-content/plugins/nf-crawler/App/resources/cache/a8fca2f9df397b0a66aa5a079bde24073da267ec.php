<div class="panel panel-default filter-item">
	<div class="panel-body">
		<div class="item">
			<?php if(isset($filter->id) && $filter->id != ''): ?>
				<input type="hidden" value="<?php echo e($filter->id); ?>" required>
			<?php endif; ?>
			<div class="form-group">
				<label>Key<span class="pull-right dashicons dashicons-no-alt" onclick="deleteFilter(this)"></span></label>
				<input type="text" class="form-control" name="filter-item-key[<?php echo e($item->id); ?>]" value="<?php echo e($item->key); ?>" required>
			</div>
			<div class="form-group">
				<label>Path</label>
				<input type="text" class="form-control" name="filter-item-path[<?php echo e($item->id); ?>]" value="<?php echo e($item->path); ?>" required>
			</div>
			<div class="form-group">
				<label>Data type</label>
				<select name="filter-item-type[<?php echo e($item->id); ?>]" class="form-control">
					<option value="text" <?php echo e($item->data_type == 'text' ? 'selected' : ''); ?>>Text</option>
					<option value="attr" <?php echo e($item->data_type == 'attr' ? 'selected' : ''); ?>>Attribute</option>
					<option value="html" <?php echo e($item->data_type == 'html' ? 'selected' : ''); ?>>HTML</option>
				</select>
			</div>
			<div class="form-group">
				<label>Attribute</label>
				<input type="text" class="form-control" name="filter-item-attribute[<?php echo e($item->id); ?>]" value="<?php echo e($item->attr); ?>">
			</div>
		</div>
	</div>
</div>