<?php
	function getPortfolio() {
		$args = [
			'post_type' => 'portfolio',
			'posts_per_page' => 20,
			'paged' => 1,
		];
		return new WP_Query($args);
	}
?>