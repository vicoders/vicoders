<?php

namespace App\NfCrawler;

use Exception;

class Post
{
    public function __construct()
    {

    }
    public static function create($post)
    {
        $post_id = wp_insert_post($post);
        if ($post_id == 0) {
            throw new Exception("Can not create new post", 1005);
        } else {
            if (isset($post['post_image'])) {
                try {
                    self::generateFeaturedImage($post['post_image'], $post_id);
                } catch (Exception $e) {

                }
            }
        }
        return $post_id;
    }
    public static function generateFeaturedImage($image_url, $post_id)
    {
        $upload_dir = wp_upload_dir();
        $image_data = file_get_contents($image_url);
        if (!isset($image_data) || $image_data == false) {
            throw new Exception("can not download image", 1007);
        } else {
            $filename = basename($image_url);
            if (wp_mkdir_p($upload_dir['path'])) {
                $file = $upload_dir['path'] . '/' . $filename;
            } else {
                $file = $upload_dir['basedir'] . '/' . $filename;
            }

            file_put_contents($file, $image_data);

            $wp_filetype = wp_check_filetype($filename, null);
            $attachment  = array(
                'post_mime_type' => $wp_filetype['type'],
                'post_title'     => sanitize_file_name($filename),
                'post_content'   => '',
                'post_status'    => 'inherit',
            );
            $attach_id = wp_insert_attachment($attachment, $file, $post_id);
            require_once ABSPATH . 'wp-admin/includes/image.php';
            $attach_data = wp_generate_attachment_metadata($attach_id, $file);
            $res1        = wp_update_attachment_metadata($attach_id, $attach_data);
            $res2        = set_post_thumbnail($post_id, $attach_id);
        }
    }
}
