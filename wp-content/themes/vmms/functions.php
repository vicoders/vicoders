<?php
define('THEME_URI', get_template_directory_uri());
require_once(__DIR__.'/includes/query.php');

/*
* regist new header-menu
*/
function register_menu() {
	register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_menu' );
/*
* add theme support custom header image
*/
$args = array(
	'default-image' => get_template_directory_uri() . '/images/logo.png',
);
add_theme_support( 'custom-header', $args );

/*
* add theme support thumbnail
*/
add_theme_support( 'post-thumbnails' ); 

/**
 * Register dynamic sidebar
 *
 */
function regist_footer_widget() {
	$widgets = [
		[
			'name' => 'Footer 1',
			'description' => 'Footer First Widget',
		],
		[
			'name' => 'Footer 2',
			'description' => 'Footer Second Widget',
		],
		[
			'name' => 'Footer 3',
			'description' => 'Footer Third Widget',
		],
		[
			'name' => 'Footer 4',
			'description' => 'Footer Fourth Widget',
		],
		[
			'name' => 'Landing Text',
			'description' => 'Landing Text',
		],
		[
			'name' => 'Block Intro 1',
			'description' => 'Block Intro',
		],
		[
			'name' => 'Block Intro 2',
			'description' => 'Block Intro',
		],
		[
			'name' => 'Block Intro 3',
			'description' => 'Block Intro',
		],
		[
			'name' => 'Block Intro 4',
			'description' => 'Block Intro',
		],
		[
			'name' => 'Portfolio Title',
			'description' => 'Portfolio Text Title',
		],
		[
			'name' => 'Comment Box Title',
			'description' => 'Comment Box Text Title',
		],
		[
			'name' => 'Portfolio Sidebar',
			'description' => 'Sidebar in portfolio page',
		],
		[
			'name' => 'People of company',
			'description' => 'people',
		],
		[
			'name' => 'Blog Sidebar',
			'description' => 'sidebar in blog page',
		],
		[
			'name' => 'Testimonials',
			'description' => 'Testimonials Section',
		],
	];
	foreach ($widgets as $key => $widget) {
		register_sidebar( array(
				'name'          => $widget['name'],
				'id'            => strtolower(str_replace(" ", "-", $widget['name'])),
				'description'   => $widget['description'],
				'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
		    ) );
	}
}
add_action( 'widgets_init', 'regist_footer_widget' );

/*
* add custom post type
*/
add_action( 'init', 'portfolio_post_type' );
function portfolio_post_type() {
  register_post_type( 'portfolio',
    array(
      'labels' => array(
		'name' => __( 'Portfolio' ),
		'singular_name' => __( 'portfolio' ),
		'all_items' => 'All',
		'add_new' => 'Add',
      ),
      'rewrite' => array('slug' => 'portfolio'),
      'public' => true,
      'has_archive' => false,
      'query_var' => 'portfolio',
      'taxonomies' => array('category','post_tag'),
      'supports' => ['title','thumbnail','editor'],
    )
  );
}
add_action( 'init', 'blog_post_type' );
function blog_post_type() {
  register_post_type( 'blog',
    array(
      'labels' => array(
		'name' => __( 'Blog' ),
		'singular_name' => __( 'blog' ),
		'all_items' => 'All',
		'add_new' => 'Add',
      ),
      'rewrite' => array('slug' => 'blog'),
      'public' => true,
      'has_archive' => false,
      'query_var' => 'blog',
      'taxonomies' => array('category','post_tag'),
      'supports' => ['title','thumbnail','editor'],
    )
  );
}

?>