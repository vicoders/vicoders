<?php

namespace App\NfCrawler;

use App\Models\Job;
use App\Models\JobFilter;
use App\Models\JobFilterItem;
use App\NfCrawler\Builder\Query;
use App\NfCrawler\Crawl;
use App\NfCrawler\Post;
use Exception;

class Admin
{
    private $blade;
    public function __construct($blade)
    {
        $this->blade = $blade;
        add_action('admin_init', function () {
            wp_enqueue_script('nf-crawler-js', plugins_url('../../assets/js/plugin.js', __FILE__));
            wp_enqueue_style('nf-crawler-css', plugins_url('../../assets/css/plugin.css', __FILE__));
            wp_enqueue_media();
        });
        add_action('admin_menu', [$this, 'pluginMenu']);
        add_action('admin_menu', [$this, 'pluginSubmenu']);
    }
    /**
     * Add Plugin Menu to Admin Panel
     */
    public function pluginMenu()
    {
        add_menu_page('Nf Crawler', 'Nf Crawler', 'manage_options', 'nf-crawler.php', [$this, 'pluginOptionPage']);
    }
    public function pluginSubmenu()
    {
        add_submenu_page('nf-crawler.php', 'Create', 'Create', 'manage_options', 'nf-crawler.php&action=create', function () {});
    }
    public function pluginOptionPage()
    {
        if (isset($_GET['action']) && $_GET['action'] != '') {
            call_user_func_array([$this, $_GET['action']], []);
        } else {
            call_user_func_array([$this, 'index'], []);
        }
    }
    /**
     * Render index page
     */
    public function index()
    {
        $jobs = Job::all();
        echo $this->blade->make('admin.index', ['jobs' => $jobs]);
    }
    /**
     * Render Create Page
     */
    public function create()
    {
        echo $this->blade->make('admin.create');
    }
    /**
     * Render Edit Page
     */
    public function edit()
    {
        $id      = $_GET['id'];
        $job     = Job::find($id);
        $filters = $job->filters();
        foreach ($filters as $key => $value) {
            $filters[$key]->items = $value->items();
        }
        $job->filters = $filters;
        echo $this->blade->make('admin.edit', ['job' => $job]);
    }
    /**
     * Create new job
     *
     * @return mixed
     */
    public function createJob()
    {
        $job       = new Job();
        $job->name = $_POST['job-name'];
        $job->url  = $_POST['job-url'];
        $job_id    = $job->insertAndReturnId();
        foreach ($_POST['filter-name'] as $key => $value) {
            $job_filter                = new JobFilter();
            $job_filter->job_id        = $job_id;
            $job_filter->name          = $_POST['filter-name'][$key];
            $job_filter->number_result = $_POST['filter-number-result'][$key];
            // $job_filter->next_filter   = $_POST['filter-result-type'][$key];
            $job_filter->insert();
        }
        $this->redirectTo();
    }
    /**
     * Update job
     *
     * @return mixed
     */
    public function updateJob()
    {
        $job       = Job::find($_POST['job-id']);
        $job->name = $_POST['job-name'];
        $job->url  = $_POST['job-url'];
        $job->update();
        foreach ($_POST['filter-name'] as $filter_id => $value) {
            if ($filter_id != 0) {
                $filter                = JobFilter::find($filter_id);
                $filter->name          = $_POST['filter-name'][$filter_id];
                $filter->number_result = $_POST['filter-number-result'][$filter_id];
                // $filter->next_filter   = $_POST['filter-result-type'][$filter_id];
                $filter->update();
            }
        }
        if (isset($_POST['filter-name']['new']) && count($_POST['filter-name']['new']) > 0) {
            foreach ($_POST['filter-name']['new'] as $key => $value) {
                $filter                = new JobFilter();
                $filter->job_id        = $job->id;
                $filter->name          = $_POST['filter-name']['new'][$key];
                $filter->number_result = $_POST['filter-number-result']['new'][$key];
                $filter->insert();
            }
        }
        if (isset($_POST['filter-item-key']) && count($_POST['filter-item-key']) > 0) {
            foreach ($_POST['filter-item-key'] as $filter_item_id => $value) {
                if ($filter_item_id != 'new') {
                    $filter_item            = JobFilterItem::find($filter_item_id);
                    $filter_item->key       = $_POST['filter-item-key'][$filter_item_id];
                    $filter_item->path      = $_POST['filter-item-path'][$filter_item_id];
                    $filter_item->data_type = $_POST['filter-item-type'][$filter_item_id];
                    $filter_item->attr      = $_POST['filter-item-attribute'][$filter_item_id];
                    $filter_item->update();
                }
            }
        }
        if (isset($_POST['filter-item-key']['new'])) {
            foreach ($_POST['filter-item-key']['new'] as $key => $value) {
                $filter_item            = new JobFilterItem();
                $filter_item->filter_id = $_POST['filter-item-filter-id']['new'][$key];
                $filter_item->key       = $_POST['filter-item-key']['new'][$key];
                $filter_item->path      = $_POST['filter-item-path']['new'][$key];
                $filter_item->data_type = $_POST['filter-item-type']['new'][$key];
                $filter_item->attr      = $_POST['filter-item-attribute']['new'][$key];
                $filter_item->insert();
            }
        }
        $this->redirectTo();
    }
    public function redirectTo($url = null)
    {
        if (!isset($url)) {
            $url = get_admin_url(null, '/admin.php?page=nf-crawler.php');
        }
        echo '<script type="text/javascript">document.location.href="' . $url . '"</script>';
    }
    public function crawl()
    {
        if (!isset($_GET['job_id']) && $_GET['job_id'] == '') {
            throw new Exception("job not found", 1004);
        } else {
            $id      = $_GET['job_id'];
            $job     = Job::find($id);
            $filters = $job->filters();
            foreach ($filters as $key => $value) {
                $filters[$key]->items = $value->items();
            }
            $job->filters     = $filters;
            $prev_filter_data = [];
            $crawled          = [];
            foreach ($job->filters as $filter) {
                if (count($prev_filter_data) > 0 && isset($prev_filter_data[0]['url'])) {
                    foreach ($prev_filter_data as $key => $value) {
                        $this->runFilter($filter, $value['url']);
                        array_push($crawled, [$filter->name => $value['url']]);
                    }
                } else {
                    $prev_filter_data = $this->runFilter($filter, $job->url);
                    array_push($crawled, [$filter->name => $job->url]);
                }

            }
            echo $this->blade->make('admin.crawl', ['job' => $job, 'crawled' => $crawled]);
        }

    }

    public function runFilter($filter, $url)
    {
        try {
            $crawl = new Crawl();
            $crawl->setUrl($url);
            if ($filter->next_filter == 0) {
                $data = $crawl->crawl($filter);
                $post = [
                    'post_title'   => $data['post_title'],
                    'post_content' => $data['post_content'],
                    'post_image'   => $data['post_image'],
                    'post_status'  => 'publish',
                ];
                $post_id = Post::create($post);
                return true;
            } else {
                $data = $crawl->crawl($filter);
                return $data;
            }
        } catch (Exception $e) {

        }
    }

    public function query()
    {
        $query = new Query();
        $query = $query->table('jobs')->where('x', '=', true)->where('y', '=', false)->get();
        var_dump($query);
    }
}
