<?php

namespace App\Models;

use App\Models\JobFilterItem;
use App\Models\Model;
use App\NfCrawler\Database;

class JobFilter extends Model
{
    protected $table = Database::DATABASE_JOB_FILTER;
    public function items()
    {
        return $this->hasMany(JobFilterItem::class, 'filter_id');
    }
}
