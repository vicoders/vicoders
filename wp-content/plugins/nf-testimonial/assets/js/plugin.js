jQuery(document).ready(function() {
    if (jQuery('.set_custom_images').length > 0) {
        if (typeof wp !== 'undefined' && wp.media && wp.media.editor) {
            jQuery('body').on('click', '.set_custom_images', function(e) {
                e.preventDefault();
                var button = jQuery(this);
                var input = button.prev();
                var image = button.prev().prev();
                wp.media.editor.send.attachment = function(props, attachment) {
                    input.val(attachment.url);
                    image.find('img').attr('src', attachment.url);
                };
                wp.media.editor.open(button);
                return false;
            });
        }
    };

});