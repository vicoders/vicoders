$(document).ready(function() {
    var Slicks = [{
        element: $('#portfolio-slick'),
        autoplay: false,
        slideNumber: {
            md: 3,
            sm: 3,
            xs: 1,
        },
    }, ];
    Slicks.forEach(function(value, key) {
        value.element.slick({
            slidesToShow: value.slideNumber.md,
            slidesToScroll: 1,
            autoplay: value.autoplay,
            autoplaySpeed: 5000,
            lazyLoad: 'ondemand',
            prevArrow: '<span class="glyphicon glyphicon-chevron-left"></span>',
            nextArrow: '<span class="glyphicon glyphicon-chevron-right"></span>',
            responsive: [{
                breakpoint: 992,
                settings: {
                    slidesToShow: value.slideNumber.sm,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 767,
                settings: {
                    slidesToShow: value.slideNumber.xs,
                    slidesToScroll: 1
                }
            }]
        });
    });
    $('#slider img:first-child').load(function() {
        scroll();
    })

    function scroll() {
        var wHeight = $(window).height();
        var wScroll = $(window).scrollTop();
        $('.paralax').each(function() {
            var elementTop = $(this).position().top;
            if (wHeight + wScroll > elementTop + 100) {
                if ($(this).hasClass('invisible')) {
                    $(this).removeClass('invisible');
                    $(this).addClass('animated');
                    $(this).addClass('fadeInUp');
                }
            }
        })
    }
    $(window).scroll(function() {
        scroll();
        caculateIntroBlockHeight();
    })

    caculateIntroBlockHeight();

    function caculateIntroBlockHeight() {
        var introBlockMaxHeight = 0;
        $('#intro-block .vmms-intro-widget').each(function() {
            if ($(this).height() > introBlockMaxHeight) {
                introBlockMaxHeight = $(this).height();
            }
        })
        $('#intro-block .vmms-intro-widget').css('min-height', introBlockMaxHeight);
    }

    scroll();

    function scroll() {
        var wHeight = $(window).height();
        var wScroll = $(window).scrollTop();
        $('.paralax').each(function() {
            var elementTop = $(this).offset().top;
            console.log(wHeight, wScroll, elementTop);
            if (wHeight + wScroll > elementTop + 100) {
                if ($(this).hasClass('invisible')) {
                    $(this).removeClass('invisible');
                    $(this).addClass('animated');
                    $(this).addClass('fadeInUp');
                }
            }
        })
    }
    $(window).scroll(function() {
        scroll();
    })
});
