<?php // If uninstall is not called from WordPress, exit

use App\NfCrawler\Database;
if (!defined('WP_UNINSTALL_PLUGIN')) {
    exit();
}
require __DIR__ . '/vendor/autoload.php';

// Drop a custom db table
global $wpdb;
$sql = "DROP TABLE IF EXISTS {$wpdb->prefix}" . Database::DATABASE_JOB_FILTER_ITEM;
$wpdb->query($sql);
$sql = "DROP TABLE IF EXISTS {$wpdb->prefix}" . Database::DATABASE_JOB_FILTER;
$wpdb->query($sql);
$sql = "DROP TABLE IF EXISTS {$wpdb->prefix}" . Database::DATABASE_JOB;
$wpdb->query($sql);
