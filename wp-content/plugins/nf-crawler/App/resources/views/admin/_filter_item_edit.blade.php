<div class="panel panel-default filter-item">
	<div class="panel-body">
		<div class="item">
			@if(isset($filter->id) && $filter->id != '')
				<input type="hidden" value="{{$filter->id}}" required>
			@endif
			<div class="form-group">
				<label>Key<span class="pull-right dashicons dashicons-no-alt" onclick="deleteFilter(this)"></span></label>
				<input type="text" class="form-control" name="filter-item-key[{{ $item->id }}]" value="{{ $item->key }}" required>
			</div>
			<div class="form-group">
				<label>Path</label>
				<input type="text" class="form-control" name="filter-item-path[{{ $item->id }}]" value="{{ $item->path }}" required>
			</div>
			<div class="form-group">
				<label>Data type</label>
				<select name="filter-item-type[{{ $item->id }}]" class="form-control">
					<option value="text" {{ $item->data_type == 'text' ? 'selected' : '' }}>Text</option>
					<option value="attr" {{ $item->data_type == 'attr' ? 'selected' : '' }}>Attribute</option>
					<option value="html" {{ $item->data_type == 'html' ? 'selected' : '' }}>HTML</option>
				</select>
			</div>
			<div class="form-group">
				<label>Attribute</label>
				<input type="text" class="form-control" name="filter-item-attribute[{{ $item->id }}]" value="{{ $item->attr }}">
			</div>
		</div>
	</div>
</div>