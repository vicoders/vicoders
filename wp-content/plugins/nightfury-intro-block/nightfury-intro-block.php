<?php
/*
Plugin Name: VMMS Intro Block Widget
Plugin URI:  http://wp.vietnamdev.com/plugins/nightfury-intro-block
Description: This plugin will help you create an accordion menu.
Version:     1.0.0
Author:      Night Fury
Author URI:  http://wp.vietnamdev.com/plugins/nightfury-bootstrap-menu
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Domain Path: http://wp.vietnamdev.com/plugins/nightfury-intro-block
Text Domain: http://wp.vietnamdev.com/plugins/nightfury-intro-block
 */
/*
* add widget css
*/
add_action('admin_head','add_pluign_nf_intro_css');
function add_pluign_nf_intro_css(){
	$output = '';
	$output .= '<link rel="stylesheet" href="' . plugins_url('css/bootstrap.min.css', __FILE__) . '">';
	$output .= '<link rel="stylesheet" href="' . plugins_url('css/bootstrap-theme.min.css', __FILE__) . '">';
	$output .= '<link rel="stylesheet" href="' . plugins_url('css/plugin.css', __FILE__) . '">';
	echo $output;
}
add_action('widgets_init', create_function('', 'register_widget("vmms_block_intro");'));
class vmms_block_intro extends WP_Widget {
	/**
	 * Constructor
	 **/
	public function __construct() {
		$widget_ops = array(
			'classname' => 'vmms-intro-widget',
			'description' => 'Intro Block Widget',
		);
		parent::__construct('vmms-intro-widget', 'Intro Block Widget', $widget_ops);
	}
	/**
	 * Outputs the HTML for this widget.
	 *
	 * @param array  An array of standard parameters for widgets in this theme
	 * @param array  An array of settings for this widget instance
	 * @return void Echoes it's output
	 **/
	public function widget($args, $instance) {
		$output = '';
		$output .= $args['before_widget'];
		if (isset($instance['icon'])) {
			$output .= '<div class="widget-intro-icon"><span class="' . $instance['icon'] . '"></span></div>';
		}
		if (isset($instance['title'])) {
			$output .= '<div class="widget-intro-title"><p>' . $instance['title'] . '</p></div>';
		}
		if (isset($instance['content'])) {
			$output .= '<div class="widget-intro-content"><p>' . $instance['content'] . '</p></div>';
		}
		$output .= $args['after_widget'];
		echo $output;
	}
	/**
	 * Deals with the settings when they are saved by the admin. Here is
	 * where any validation should be dealt with.
	 *
	 * @param array  An array of new settings as submitted by the admin
	 * @param array  An array of the previous settings
	 * @return array The validated and (if necessary) amended settings
	 **/
	public function update($new_instance, $old_instance) {
		// update logic goes here
		$updated_instance = $new_instance;
		return $updated_instance;
	}
	/**
	 * Displays the form for this widget on the Widgets page of the WP Admin area.
	 *
	 * @param array  An array of the current settings for this widget
	 * @return void
	 **/
	public function form($instance) {
		?>
		<div id="intro-widget">
			<div class="form-group">
				<label>Icon</label>
				<input type="text" class="form-control" id="<?php echo $this->get_field_id('icon');?>" name="<?php echo $this->get_field_name('icon');?>" value="<?php echo isset($instance['icon']) ? $instance['icon'] : '';?>">
			</div>
			<div class="form-group">
				<label>Title</label>
				<input type="text" class="form-control" id="<?php echo $this->get_field_id('title');?>" name="<?php echo $this->get_field_name('title');?>" value="<?php echo isset($instance['title']) ? $instance['title'] : '';?>">
			</div>
			<div class="form-group">
				<label>Content</label>
				<textarea class="form-control" id="<?php echo $this->get_field_id('content');?>" name="<?php echo $this->get_field_name('content');?>"><?php echo isset($instance['content']) ? $instance['content'] : '';?></textarea>
			</div>
		</div>
	<?php
	}
}
?>