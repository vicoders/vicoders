function addMoreFilter() {
    var clone = jQuery('#new-filter .filter-item').clone();
    clone.find('input').val('');
    clone.insertBefore(jQuery('#add-more-filter'));
}

function addMoreFilterItem(element, filter_id) {
    var clone = jQuery('#new-filter-item > div').clone();
    var filter_id_input = jQuery('<input type="hidden" name="filter-item-filter-id[new][]" required>');
    filter_id_input.val(filter_id);
    clone.find('.item').append(filter_id_input);
    clone.insertBefore(jQuery(element).parent());
}

function deleteFilter(element) {
    var length = jQuery('#nf-crawler-filters .filter-item').length;
    var deleteElement = jQuery(element).parent().parent().parent().parent().parent();
    if (deleteElement.hasClass('filter-item')) {
        deleteElement.remove();
    }
}

function run(job_id) {
    var job;
    var data = {
        action: 'job',
        job_id: job_id,
    };
    var job;
    jQuery.ajax({
            type: "POST",
            url: ajaxurl,
            data: data,
            async: false,
        })
        .success(function(response) {
            job = response.data;
        })
        .error(function(response) {
            console.log(response);
        });
    // var prev_filter_data;
    // job.filters.forEach(function(filter) {
    //     if (prev_filter_data !== undefined && prev_filter_data.length > 0 && prev_filter_data[0].url !== undefined) {
    //         prev_filter_data.forEach(function(value) {
    //             try {
    //                 runFilter(filter, value.url);
    //                 console.log(value.url);
    //             } catch (e) {
    //                 console.log(e);
    //             }
    //         })
    //     } else {
    //         prev_filter_data = runFilter(filter, job.url);
    //     }
    // })
    runFilter(job.filters[0], job.url);
}

function runFilter(filter, url) {
    var data = {
        action: 'run_filter',
        filter_id: filter.id,
        url: url,
        post_status: 'draft',
        post_type: 'blog',
        category_ids: [6, 7],
    };
    var filterData;
    jQuery.ajax({
            type: "POST",
            url: ajaxurl,
            data: data,
            async: false,
        })
        .success(function(response) {
            filterData = response.data;
            console.log(filter.next_filter, typeof filter.next_filter, filter.next_filter !== 0);
            if (filter.next_filter !== '0') {
                filterData.forEach(function(value) {
                    runFilter(filter.next_filter, value.url);
                })
            }
        });
    return filterData;
}
