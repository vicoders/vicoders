<div class="panel panel-default filter-item">
	<div class="panel-body">
		<div class="item">
			<div class="form-group">
				<label>Key<span class="pull-right dashicons dashicons-no-alt" onclick="deleteFilter(this)"></span></label>
				<input type="text" class="form-control" name="filter-item-key[new][]" required>
			</div>
			<div class="form-group">
				<label>Path</label>
				<input type="text" class="form-control" name="filter-item-path[new][]" required>
			</div>
			<div class="form-group">
				<label>Data type</label>
				<select name="filter-item-type[new][]" class="form-control">
					<option value="text">Text</option>
					<option value="attr">Attribute</option>
					<option value="html">HTML</option>
				</select>
			</div>
			<div class="form-group">
				<label>Attribute</label>
				<input type="text" class="form-control" name="filter-item-attribute[new][]">
			</div>
		</div>
	</div>
</div>