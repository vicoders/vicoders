<?php
class BootstrapAccordionMenuWalker extends Walker_Nav_Menu
{
	private $curItem;
	function start_lvl( &$output, $item, $depth = 0, $args = array(), $id = 0 )
	{
		$output .= '<div class="accordion-menu collapse" id="accordion-' . $this->curItem->ID . '">';
		// echo '<script type="text/javascript">';
		// echo 'var item=' . json_encode($this->curItem) .';';
		// echo 'console.log(item);';
		// echo "</script>";
	}
	function end_lvl( &$output, $item, $depth = 0, $args = array(), $id = 0 )
	{
		$output .= '</div>';
	}
	function start_el( &$output, $item, $depth, $args )
	{
		$this->curItem = $item;

		if($item->attr_title == 'parent')
		{
			$output .= '<div class="accordion"><div class="accordion-heading" href="#accordion-' . $item->ID . '" data-toggle="collapse" aria-expanded="false"><a href="' . $item->url . '">' . $item->title . '</a></div>';
		}
		else
		{
			$output .= '<li class="accordion-item" data="' . $item->ID . '"><a href="' . $item->url . '">' . $item->title . '</a>';
		}
	}
	function end_el( &$output, $item, $depth, $args )
	{
		if($item->attr_title == 'parent')
		{
			$output .= '</div>';
		}
		else
		{
			$output .= '</li>';
		}
	}
}
?>