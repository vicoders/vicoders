<?php get_header(); ?>
	<?php while(have_posts()) : the_post() ?>	
	<div id="about" class="container">
		<div class="title">
			<h1><?php the_title(); ?></h1>
		</div>
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">	
			<div class="about">
				<?php the_content(); ?>
			</div>
		</div>	
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<div class="background-white">
				<?php dynamic_sidebar('blog-sidebar');	?>	
			</div>
		</div>
	</div>	
	<?php endwhile; ?>

<?php get_footer(); ?>