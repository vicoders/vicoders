<?php

namespace App\Models;

use App\Models\Model;
use App\NfCrawler\Database;

class JobFilterItem extends Model
{
    protected $table = Database::DATABASE_JOB_FILTER_ITEM;
}
