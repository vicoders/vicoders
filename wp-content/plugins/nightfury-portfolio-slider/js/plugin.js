function slide_init(slides) {
    console.log(slides);
    var key = 0;
    setSlide(slides, key);
    setInterval(function() {
        deleteSlide(slides, key);
        key++;
        if (key == slides.length) {
            key = 0;
        }
        setSlide(slides, key);
    }, 6000);
};

function setSlide(slides, key) {
    var div = document.createElement('div');
    div.className = 'slide';
    var img = document.createElement('img');
    img.className = 'animated fadeIn';
    img.src = slides[key].image;
    div.appendChild(img);
    document.getElementById('image-slider').appendChild(div);
    slides[key].text.forEach(function(value, key) {
        var li = document.createElement('li');
        li.style.animationDelay = (key / 10) + 's';
        li.className = 'animated fadeInRightBig';
        var div = document.createElement('div');
        div.innerHTML = value;
        li.appendChild(div);
        document.getElementById('nf-text').appendChild(li);
    });
};

function deleteSlide(slides, key) {
    setTimeout(function() {
        var imagelist = document.getElementById('image-slider');
        imagelist.removeChild(imagelist.firstChild);
    }, 500);
    var list = document.getElementById("nf-text");
    while (list.hasChildNodes()) {
        list.removeChild(list.firstChild);
    }
};
