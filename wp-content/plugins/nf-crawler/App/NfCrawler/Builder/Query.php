<?php

namespace App\NfCrawler\Builder;

class Query
{
    public $table;
    public $where = [];
    public $query;
    public $columns = ['*'];
    public $statement;
    public $operators = ['=', '<', '>', '<=', '>=', '<>', 'like'];
    public function __construct()
    {

    }
    public function query()
    {

    }
    /**
     * add statement to query
     */
    public function addStatementToQuery()
    {
        $this->query = $this->statement;
    }
    /**
     * Add selected columns to query
     */
    public function addColumnsToQuery()
    {
        $columns = $this->columns;
        $query   = $this->query;
        foreach ($columns as $key => $value) {
            if ($value == end($columns)) {
                $query .= ' ' . $value;
            } else {
                $query .= ' ' . $value . ',';
            }
        }
        $this->query = $query;
    }
    /**
     * Add FROM clause to query
     */
    public function addFromToQuery()
    {
        $this->query .= ' FROM ' . $this->table;
    }
    /**
     * Add WHERE clause to query
     */
    public function addWhereToQuery()
    {
        $where = $this->where;
        if (count($where) == 0) {
            return false;
        } else {
            $query = $this->query;
            $query .= ' WHERE';
            foreach ($where as $key => $value) {
                if ($value == end($where)) {
                    $query .= ' `' . $value['column'] . '` ' . $value['operator'] . ' \'' . $value['value'] . '\'';
                } else {
                    $query .= ' `' . $value['column'] . '` ' . $value['operator'] . ' \'' . $value['value'] . '\' AND';
                }
            }
            $this->query = $query;
        }
    }
    public function get()
    {
        $this->statement = 'SELECT';
        $this->addStatementToQuery();
        $this->addColumnsToQuery();
        $this->addFromToQuery();
        $this->addWhereToQuery();
        return $this->query;
    }
    public function table($table)
    {
        $this->table = $table;
        return $this;
    }
    public function where($column, $operator, $value)
    {
        if (!in_array($operator, $this->operators)) {
            throw new Exception("operator not valid", 1003);
        } else {
            array_push($this->where, ['column' => $column, 'operator' => $operator, 'value' => $value]);
        }
        return $this;
    }
}
