<?php get_header(); ?>
	<div id="blog" class="container">
		<div class="title"><h1><?php the_title(); ?></h1></div>
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">		
			<?php $args = array(
					'posts_per_page' => 10 ,
					'post_type' => 'blog',
					'paged' => 1,

				);
			   $blogs = new WP_Query($args);
			   while ($blogs->have_posts()) : $blogs->the_post();
			   // var_dump($post);
			?>
				<div class="blog">					
					<div class="blog-image col-md-4 col-sm-4 col-xs-12">
						<div class="img-thumb">
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<?php the_post_thumbnail( 'thumbnail' );   ?>
							</a>
						</div>
					</div>			
					<div class="blog-text col-md-8 col-sm-8 col-xs-12">
						<div class="blog-title">
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<h2><?php the_title(); ?></h2></a>
							<div class="blog-info">
								<ul>
									<li><span class="glyphicon glyphicon-time"></span><span><?php echo get_the_date(); ?></span></li>
									<li><span class="glyphicon glyphicon-user"></span><span><?php echo get_the_author(); ?></span></li>
								</ul>
							</div>
						</div>
						<div class="blog-content">			
							<?php echo substr(strip_tags(get_the_content()),0,200); ?>
							<a href="<?php echo get_permalink(); ?>">[...]</a>
						</div>
					</div>
					
				</div>
			<?php endwhile; ?>		
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<div class="background-white">
				<?php dynamic_sidebar('blog-sidebar');	?>	
			</div>
		</div>
	</div>	
<?php get_footer(); ?>