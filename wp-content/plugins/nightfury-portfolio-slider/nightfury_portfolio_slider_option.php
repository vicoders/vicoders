<?php
function add_media_upload_script() {
	wp_enqueue_script('media-upload');
	wp_enqueue_script('thickbox');
	wp_enqueue_script('upload_media_widget', plugin_dir_url(__FILE__) . 'js/upload-media.js', array('jquery'));

	wp_enqueue_style('thickbox');
}
add_action('admin_enqueue_scripts', 'add_media_upload_script');
/*
*	Add new slide
*/

if (isset($_POST['image'])) {
	$option = get_option(NF_PORTFOLIO_SLIDER_OPTION_KEY);
	if ($option == false) {
		add_option(NF_PORTFOLIO_SLIDER_OPTION_KEY, [$_POST], '', 'yes');
	} else {
		array_push($option, $_POST);
		update_option(NF_PORTFOLIO_SLIDER_OPTION_KEY, $option, '', 'yes');
	}
}
/*
* DELETE SLIDE
*/
if(isset($_POST['delete_slide_id'])) {
	$option = get_option(NF_PORTFOLIO_SLIDER_OPTION_KEY);
	if($option != false && isset($option[$_POST['delete_slide_id']])) {
		if(count($option) == 1) {
			delete_option(NF_PORTFOLIO_SLIDER_OPTION_KEY);
		} else {
			array_splice($option, $_POST['delete_slide_id'], 1);
			update_option(NF_PORTFOLIO_SLIDER_OPTION_KEY, $option, '', 'yes');
		}
	}
}
/*
* OPTION PAGE
*/
function nightfury_portfolio_slider_option() {
	$option = get_option(NF_PORTFOLIO_SLIDER_OPTION_KEY);
	?>
	<div class="col-md-12 col-sm-12 col-xs-12" id="nf-portfolio-slider-admin">
		<?php if ($option != falses) {
			foreach($option as $key => $slide) { ?>
				<div class="panel panel-default">
					<div class="panel-heading"><?php echo '#'.($key + 1); ?></div>
					<div class="panel-body">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="nf-images-list">
									<img src="<?php echo plugins_url('images/macbook.png', __FILE__);?>">
									<div class="image-slider upload_image_button">
										<img class="update-image-data" src="<?php echo $slide['image']; ?>" alt="">
									</div>
								</div> <!-- .upload_image_button -->
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="nf-text">
									<ul class="nf-update-text-list">
										<?php foreach ($slide['text'] as $text) {
											echo '<li>'. $text .'</li>';
										} ?>
									</ul>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" class="update-text-data">
								</div>
								<div class="form-group">
									<button class="btn btn-default" onclick="AddText()"><span class="glyphicon glyphicon-plus"></span> Add Text</button>
								</div>
							</div>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<button class="btn btn-info" onclick="UpdateSlide(<?php echo $key; ?>)">Update</button>
							<button class="btn btn-warning" onclick="DeleteSlide(<?php echo $key; ?>)">Delete</button>
						</div>
					</div>
				</div>
		<?php }	} ?>
		<div class="panel panel-default">
			<div class="panel-heading">Add New Slide</div>
			<div class="panel-body">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="nf-images-list">
							<img src="<?php echo plugins_url('images/macbook.png', __FILE__);?>">
							<div class="image-slider upload_image_button">
								<img id="image-data" src="" alt="">
							</div>
						</div> <!-- .upload_image_button -->
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="nf-text">
							<ul id="nf-text-list"></ul>
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="text-data">
						</div>
						<div class="form-group">
							<button class="btn btn-default" onclick="AddText()"><span class="glyphicon glyphicon-plus"></span> Add Text</button>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12">
					<button class="btn btn-primary" onclick="AddNewSlide()">Save</button>
				</div>
			</div>
		</div>
	</div>
<?php
}
?>