<?php
/*
Plugin Name: NightFury Portfolio Slider
Plugin URI:  http://wp.vietnamdev.com/plugins/nightfury-portfolio-slider
Description: Special Slider :D
Version:     1.0.0
Author:      Night Fury
Author URI:  http://wp.vietnamdev.com/plugins/nightfury-portfolio-slider
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Domain Path: http://wp.vietnamdev.com/plugins/nightfury-portfolio-slider
Text Domain: http://wp.vietnamdev.com/plugins/nightfury-portfolio-slider
 */
defined('ABSPATH') or die('No script kiddies please!');

define('NF_PORTFOLIO_SLIDER_OPTION_KEY', 'nf_portfolio_slider_option_key');
/*
 * Register custom menu
 */
add_action('admin_menu', 'nightfury_portfolio_slider_admin_menu');

function nightfury_portfolio_slider_admin_menu() {
	add_menu_page('NightFury Slider', 'NightFury Slider', 'manage_options', 'nightfury-portfolio-slider', 'nightfury_portfolio_slider_option');
}

/*
 * add css to wp admin
 */
add_action('admin_head', 'nf_portfolio_add_css');
function nf_portfolio_add_css() {
	$output = '';
	$output .= '<link rel="stylesheet" href="' . plugins_url('css/bootstrap.min.css', __FILE__) . '">';
	$output .= '<link rel="stylesheet" href="' . plugins_url('css/bootstrap-theme.min.css', __FILE__) . '">';
	$output .= '<link rel="stylesheet" href="' . plugins_url('css/plugin.css', __FILE__) . '">';
	echo $output;
}
require_once __DIR__ . '/nightfury_portfolio_slider_option.php';

add_action('wp_head','nf_portfolio_slider_wp_head');
function nf_portfolio_slider_wp_head(){
	$output = '';
	$output .= '<link type="text/css" href="' . plugins_url('css/plugin-page.css', __FILE__) . '">';
	$output .= '<script src="' . plugins_url('js/plugin.js', __FILE__) . '"></script>';
	echo $output;
}

function nightfury_portfolio_slider() {
?>
	<script type="text/javascript">
		var nf_slides = <?php echo json_encode(get_option('nf_portfolio_slider_option_key')); ?>;
	</script>
	<div id="nf-portfolio-slider">
		<?php $slides = get_option('nf_portfolio_slider_option_key'); ?>
		<div class="half">
			<div class="nf-image">
				<div class="relative animated fadeInLeftBig">
					<img class="frame" src="<?php echo THEME_URI; ?>/images/macbook.png" alt="macbook">
					<div id="image-slider"></div>
				</div>
			</div>
		</div>
		<div class="half">
			<ul id="nf-text"></ul>
		</div>
	</div>
	<script type="text/javascript">
		slide_init(nf_slides);
	</script>
<?php
}
?>